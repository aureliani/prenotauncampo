package it.uniroma3.controller;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.controller.validator.UtenteValidator;
import it.uniroma3.model.Prenotazione;
import it.uniroma3.model.Utente;
import it.uniroma3.service.UtenteService;

@Controller
public class LoginController {

	@Autowired
	private UtenteService utenteService;

	@Autowired
	private UtenteValidator validator;

	@RequestMapping("/login")
	public String login(Model model, HttpSession session) {
		model.addAttribute("user", new Utente());
		return "login";
	}

	@RequestMapping("/role")
	public String loginRole(HttpSession session, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String role = auth.getAuthorities().toString();
		Utente user = this.utenteService.findByUsername(auth.getName());
		System.out.println("\n"+user.getUsername()+"\n");

		if(role.contains("ROLE_USER")) {
			session.setAttribute("user", user);
			return "redirect:/benvenuto";
		} else if(role.contains("ROLE_ADMIN")) {
			session.setAttribute("user", user);
			return "redirect:/prenotazioni";
		}
		return "redirect:/benvenuto";
	}

	@RequestMapping("/errore403")
	public String error403() {
		return "errore403";
	}

	@RequestMapping(value = "/registrazioneUtente", method = RequestMethod.POST)
	public String nuovoUtente(@ModelAttribute("user") Utente utente, Model model, BindingResult bindingResult, HttpSession session) {  
		System.out.println("\n\nERROR\n\n");
		this.validator.validate(utente, bindingResult);
		utente.setRole("ROLE_USER");
		if (this.utenteService.alreadyExists(utente)) {
			System.out.println("\n\nERROR\n\n");
			model.addAttribute("message", "Questo username è già presente");
		}
		else {
			if (!bindingResult.hasErrors()) {
				System.out.println("\n\nERROR\n\n");
				BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
				utente.setPassword(bCryptPasswordEncoder.encode(utente.getPassword()));
				utenteService.save(utente);
				session.setAttribute("user", utente);
			}
		}
		return "redirect:/login";
	}
}