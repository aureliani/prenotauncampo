package service;

import java.util.List;

public interface JpaRepositoryInterface<T> {
	public T save(T c);
	public T findByPrimaryKey(Long id);
	public List<T> findAll();
	public void update(T c);
	public void delete(T c);

}