package repository;

import java.util.List;
import model.Prenotazione;

public interface PrenotazioneRepository {

	public Prenotazione save (Prenotazione p);
	public Prenotazione findByPrimaryKey(Long idPrenotazione);
	public List<Prenotazione> findAll();
	public void update(Prenotazione p);
	public void delete(Prenotazione p);
}
