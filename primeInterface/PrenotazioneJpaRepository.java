package repository.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import model.Prenotazione;
import repository.PrenotazioneRepository;

public class PrenotazioneJpaRepository implements PrenotazioneRepository {

	private EntityManagerFactory emf;
	private EntityManager em;

	public PrenotazioneJpaRepository(EntityManager em) {
		this.em = em;
	}

	@Override
	public Prenotazione save(Prenotazione p) {
		if(p.getId() == null) 
		{
			em.persist(p);
		}
		else 
		{
			Long prenotazionePersistita = p.getId();
			if(prenotazionePersistita == null) 
			{
				em.persist(p);
			}
			else 
			{
				update(p);
			}
		}
		return p;
	}

	@Override
	public Prenotazione findByPrimaryKey(Long idPrenotazione) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Prenotazione> findAll() {
		List<Prenotazione> prenotazioni = em.createNamedQuery("findAllPrenotazioni").getResultList();
		return prenotazioni;
	}

	@Override
	public void update(Prenotazione p) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Prenotazione p) {
		// TODO Auto-generated method stub
	}
}