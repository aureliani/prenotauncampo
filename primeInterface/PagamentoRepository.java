package repository;

import java.util.List;

import model.Pagamento;

public interface PagamentoRepository {

	public Pagamento save (Pagamento p);
	public Pagamento findByPrimaryKey(Long idPagamento);
	public List<Pagamento> findAll();
	public void update(Pagamento p);
	public void delete(Pagamento p);

}
