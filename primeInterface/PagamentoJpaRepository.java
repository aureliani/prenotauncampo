package repository.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import model.Pagamento;
import repository.PagamentoRepository;

public class PagamentoJpaRepository implements PagamentoRepository {

	private EntityManagerFactory emf;
	private EntityManager em;

	public PagamentoJpaRepository(EntityManager em) {
		this.em = em;
	}

	@Override
	public Pagamento save(Pagamento p) {
		if(p.getId() == null) 
		{
			em.persist(p);
		}
		else 
		{
			Long pagamentoPersistito = p.getId();
			if(pagamentoPersistito == null) 
			{
				em.persist(p);
			}
			else 
			{
				update(p);
			}
		}
		return p;
	}

	@Override
	public Pagamento findByPrimaryKey(Long idPagamento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pagamento> findAll() {
		List<Pagamento> pagamenti = em.createNamedQuery("findAllPagamenti").getResultList();
		return pagamenti;
	}

	@Override
	public void update(Pagamento p) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Pagamento p) {
		// TODO Auto-generated method stub

	}
}