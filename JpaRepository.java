package service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.persistence.EntityManager;



public class JpaRepository<T> implements JpaRepositoryInterface<T> {
	
	private EntityManager em;
	private Class<T> classObj;
	
	public JpaRepository(EntityManager em , Class<T> c){
		this.em = em;
		this.classObj = c;
	}
	
	@Override
	public T save(T element){
		
		try 
		{
			Method getId = classObj.getDeclaredMethod("getId", new Class[]{});
			Long id = (Long)getId.invoke(element, new Object[]{});
			
			if(id==null) 
			{
				em.persist(element);
			}
			else
			{
				T persistedElem = findByPrimaryKey(id);
				if (persistedElem==null)
				{
					em.persist(element);
				}
				else 
				{
					em.merge(element);
				}
			}
			return element;
		}
		catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) 
		{
			return null;
		}
	}

	@Override
	public T findByPrimaryKey(Long id) {
		return em.find(classObj,id);
	}

	@Override
	public List<T> findAll() {
		return em.createNamedQuery("SELECT e FROM "+classObj.getName()+" e", classObj).getResultList();
	}

	@Override
	public void update(T element) {
		em.merge(element);
	}

	@Override
	public void delete(T element) {
		em.remove(element);		
	}
}