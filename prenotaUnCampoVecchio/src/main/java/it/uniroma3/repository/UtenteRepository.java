package it.uniroma3.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import it.uniroma3.model.Utente;

public interface UtenteRepository extends CrudRepository <Utente, Long> {

	public void delete(Utente u);
	public long count();
	boolean existsById(Long id);
	Iterable<Utente> findAll();
	Optional<Utente> findById(Long id);
	List<Utente> findByNome(String nome);
	List<Utente> findByCognome(String cognome);
	Utente findByUsername(String username);
}