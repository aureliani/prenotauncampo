package it.uniroma3.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import it.uniroma3.model.Pagamento;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

	public void delete(Pagamento d);
	public long count();
	boolean existsById(Long id);
	Iterable<Pagamento> findAll();
	Optional<Pagamento> findById(Long id);
}