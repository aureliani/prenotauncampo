package it.uniroma3.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import it.uniroma3.model.Campo;

public interface CampoRepository extends CrudRepository<Campo, Long> {
	
	public void delete(Campo c);
	public long count();
	boolean existsById(Long id);
	Optional<Campo> findById(Long id);
	List<Campo> findAll();
	List<Campo> findByNome(String nome);
}