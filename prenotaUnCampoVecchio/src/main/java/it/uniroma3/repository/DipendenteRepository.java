package it.uniroma3.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import it.uniroma3.model.Dipendente;

public interface DipendenteRepository extends CrudRepository<Dipendente,Long> {

	public void delete(Dipendente d);
	public long count();
	boolean existsById(Long id);
	Iterable<Dipendente> findAll();
	Optional<Dipendente> findById(Long id);
	List<Dipendente> findByNome(String nome);
}