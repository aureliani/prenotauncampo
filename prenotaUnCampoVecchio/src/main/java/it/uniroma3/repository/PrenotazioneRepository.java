package it.uniroma3.repository;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import it.uniroma3.model.Prenotazione;

public interface PrenotazioneRepository extends CrudRepository<Prenotazione, Long> {

	public void delete(Prenotazione c);
	public long count();
	boolean existsById(Long id);
//	Iterable<Prenotazione> findAll();
	Optional<Prenotazione> findById(Long id);
	List<Prenotazione> findByData(Calendar data);
}