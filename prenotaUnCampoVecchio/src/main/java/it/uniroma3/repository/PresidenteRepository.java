package it.uniroma3.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import it.uniroma3.model.Presidente;

public interface PresidenteRepository extends CrudRepository<Presidente, Long> {

	public void delete(Presidente c);
	public long count();
	boolean existsById(Long id);
	Iterable<Presidente> findAll();
	Optional<Presidente> findById(Long id);
	List<Presidente> findByNome(String nome);
}