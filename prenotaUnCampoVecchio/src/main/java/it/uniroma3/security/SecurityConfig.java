package it.uniroma3.security;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final String usersQuery = "SELECT username,password,TRUE FROM utente WHERE username = ?";
    private final String rolesQuery = "SELECT username,role FROM utente WHERE username = ?";

    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder())
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder())
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery);
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
		        .csrf().disable()
		        .authorizeRequests()
		        .antMatchers("/registrazioneUtente", "/login", "/registration").permitAll()
		        .antMatchers("/", "/index", "/chisiamo", "/circoli").permitAll()
		        .antMatchers("/prenotazioniList").hasRole("ADMIN")
		        .anyRequest().permitAll()
		        .anyRequest().authenticated()
		        .and()
		        .formLogin()
		        .loginPage("/login")
		        .defaultSuccessUrl("/role")
		        .and()
		        .logout()
		        .logoutSuccessUrl("/login")
		        .permitAll()
		        .and()
		        .exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    }

    //Spring Boot configured this already.
    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers("/static/**", "/style.css" , "/assets/**", "/css/**", "/images/**", "/js/**");
    }
}