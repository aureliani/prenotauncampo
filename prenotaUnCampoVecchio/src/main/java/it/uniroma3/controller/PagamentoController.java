package it.uniroma3.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.controller.validator.UtenteValidator;
import it.uniroma3.model.Pagamento;
import it.uniroma3.service.PagamentoService;

@Controller
public class PagamentoController {

	@Autowired
	private PagamentoService pagamentoService;

	@Autowired
	private UtenteValidator validator;

//	@RequestMapping("/pagamenti")
//	public String pagamenti(Model model) {
//		model.addAttribute("pagamenti", this.pagamentoService.findAll());
//		return "pagamentiList";
//	}
//
//	@RequestMapping("/addPagamento")
//	public String addPagamento(Model model) {
//		model.addAttribute("pagamento", new Pagamento());
//		return "pagamentoForm";
//	}
//
//	@RequestMapping(value = "/pagamento/{id}", method = RequestMethod.GET)
//	public String getPagamento(@PathVariable("id") Long id, Model model) {
//		model.addAttribute("pagamento", this.pagamentoService.findById(id));
//		return "showPagamento";
//	}
//
//	@RequestMapping(value = "/pagamento", method = RequestMethod.POST)
//	public String newPagamento(@Valid @ModelAttribute("pagamento") Pagamento pagamento, 
//			Model model, BindingResult bindingResult) {
//		this.validator.validate(pagamento, bindingResult);
//
//		if (this.pagamentoService.alreadyExists(pagamento)) {
//			model.addAttribute("exists", "Pagamento already exists");
//			return "pagamentoForm";
//		}
//		else {
//			if (!bindingResult.hasErrors()) {
//				this.pagamentoService.save(pagamento);
//				model.addAttribute("pagamenti", this.pagamentoService.findAll());
//				return "utentiList";
//			}
//		}
//		return "pagamentoForm";
//	}

}