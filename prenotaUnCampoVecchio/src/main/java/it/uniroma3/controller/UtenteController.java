package it.uniroma3.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.controller.validator.UtenteValidator;
import it.uniroma3.model.Utente;
import it.uniroma3.service.UtenteService;

@Controller
public class UtenteController {

	@Autowired
	private UtenteService utenteService;

	@Autowired
	private UtenteValidator validator;
//
//	@RequestMapping("/utenti")
//	public String utenti(Model model) {
//		model.addAttribute("utenti", this.utenteService.findAll());
//		return "utentiList";
//	}
//
//
//	@RequestMapping("/addUtente")
//	public String addUtente(Model model) {
//		model.addAttribute("utente", new Utente());
//		return "utenteForm";
//	}
//
//	@RequestMapping(value = "/utente/{id}", method = RequestMethod.GET)
//	public String getCustomer(@PathVariable("id") Long id, Model model) {
//		model.addAttribute("utente", this.utenteService.findById(id));
//		return "showUtente";
//	}
//
//	@RequestMapping(value = "/utente", method = RequestMethod.POST)
//	public String newCustomer(@Valid @ModelAttribute("utente") Utente utente, 
//			Model model, BindingResult bindingResult) {
//		this.validator.validate(utente, bindingResult);
//
//		if (this.utenteService.alreadyExists(utente)) {
//			model.addAttribute("exists", "Utente already exists");
//			return "utenteForm";
//		}
//		else {
//			if (!bindingResult.hasErrors()) {
//				this.utenteService.save(utente);
//				model.addAttribute("utenti", this.utenteService.findAll());
//				return "utentiList";
//			}
//		}
//		return "utenteForm";
//	}

}