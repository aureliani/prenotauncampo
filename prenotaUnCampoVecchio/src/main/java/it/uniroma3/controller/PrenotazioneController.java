package it.uniroma3.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import it.uniroma3.controller.validator.PrenotazioneValidator;
import it.uniroma3.model.Campo;
import it.uniroma3.model.Prenotazione;
import it.uniroma3.model.Utente;
import it.uniroma3.service.CampoService;
import it.uniroma3.service.PrenotazioneService;

@Controller
public class PrenotazioneController {

	@Autowired
	private PrenotazioneService prenotazioneService;

	@Autowired
	private CampoService campoService;

	@Autowired
	private PrenotazioneValidator validator;


	@RequestMapping("/prenotazioni")
	public String customers(Model model) {
		model.addAttribute("prenotazioni", this.prenotazioneService.findAll());
		return "prenotazioniList";
	}

	@RequestMapping("/addPrenotazione")
	public String addPrenotazione(@RequestParam(name = "campoSelect") Long idCampo, Model model) {
		//model.addAttribute("prenotazione", new Prenotazione());
		Campo campo = campoService.findById(idCampo);
		Prenotazione prenotazione = new Prenotazione();
		prenotazione.setCampo(campo);
		prenotazioneService.save(prenotazione);
		return "redirect:/benvenuto";
	}

	@RequestMapping(value = "/prenotazione/{id}", method = RequestMethod.GET)
	public String getPrenotazione(@PathVariable("id") Long id, Model model) {
		model.addAttribute("prenotazione", this.prenotazioneService.findById(id));
		return "showPrenotazione";
	}

	@RequestMapping(value = "/prenotazione", method = RequestMethod.POST)
	public String newPrenotazione(@Valid @ModelAttribute("prenotazione") Prenotazione prenotazione, 
			Model model, BindingResult bindingResult) {
		this.validator.validate(prenotazione, bindingResult);

		if (this.prenotazioneService.alreadyExists(prenotazione)) {
			model.addAttribute("exists", "Prenotazione already exists");
			return "prenotazioneForm";
		}
		else {
			if (!bindingResult.hasErrors()) {
				this.prenotazioneService.save(prenotazione);
				model.addAttribute("prenotazioni", this.prenotazioneService.findAll());
				return "prenotazioniList";
			}
		}
		return "prenotazioneForm";
	}

}