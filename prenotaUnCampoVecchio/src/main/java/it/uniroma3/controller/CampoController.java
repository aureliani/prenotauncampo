package it.uniroma3.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.controller.validator.CampoValidator;
import it.uniroma3.model.Campo;
import it.uniroma3.service.CampoService;

@Controller
public class CampoController {

	@Autowired
	private CampoService campoService;

	@Autowired
	private CampoValidator validator;

//	@RequestMapping("/campo")
//	public String campo(Model model) {
//		model.addAttribute("campi", this.campoService.findAll());
//		return "campiList";
//	}
//	
//	@RequestMapping("/addCampo")
//	public String addCampo(Model model) {
//		model.addAttribute("campo", new Campo());
//		return "campoForm";
//	}
//
//	@RequestMapping(value = "/campo/{id}", method = RequestMethod.GET)
//	public String getCampo(@PathVariable("id") Long id, Model model) {
//		model.addAttribute("campo", this.campoService.findById(id));
//		return "showCampo";
//	}
//
//	@RequestMapping(value = "/campo", method = RequestMethod.POST)
//	public String newCustomer(@Valid @ModelAttribute("campo") Campo campo, 
//			Model model, BindingResult bindingResult) {
//		this.validator.validate(campo, bindingResult);
//
//		if (this.campoService.alreadyExists(campo)) {
//			model.addAttribute("exists", "Campo already exists");
//			return "campoForm";
//		}
//		else {
//			if (!bindingResult.hasErrors()) {
//				this.campoService.save(campo);
//				model.addAttribute("campi", this.campoService.findAll());
//				return "campiiList";
//			}
//		}
//		return "campoForm";
//	}

}