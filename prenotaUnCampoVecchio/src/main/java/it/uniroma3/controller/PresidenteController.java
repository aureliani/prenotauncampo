package it.uniroma3.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.controller.validator.PresidenteValidator;
import it.uniroma3.model.Presidente;
import it.uniroma3.service.PresidenteService;

@Controller
public class PresidenteController {

	@Autowired
	private PresidenteService presidenteService;

//	@Autowired
//	private PresidenteValidator validator;

//	@RequestMapping("/addPresidente")
//	public String addPresidente(Model model) {
//		model.addAttribute("presidente", new Presidente());
//		return "presidenteForm";
//	}
//
//	@RequestMapping(value = "/presidente/{id}", method = RequestMethod.GET)
//	public String getPresidente(@PathVariable("id") Long id, Model model) {
//		model.addAttribute("utente", this.presidenteService.findById(id));
//		return "showPresidente";
//	}
}