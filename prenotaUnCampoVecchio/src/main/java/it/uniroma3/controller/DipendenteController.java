package it.uniroma3.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.controller.validator.DipendenteValidator;
import it.uniroma3.model.Dipendente;
import it.uniroma3.service.DipendenteService;

@Controller
public class DipendenteController {

	@Autowired
	private DipendenteService dipendenteService;

	@Autowired
	private DipendenteValidator validator;

//	@RequestMapping("/dipendenti")
//	public String dipendenti(Model model) {
//		model.addAttribute("dipendenti", this.dipendenteService.findAll());
//		return "dipendentiList";
//	}
//
//	@RequestMapping("/addDipendente")
//	public String addDipendente(Model model) {
//		model.addAttribute("dipendente", new Dipendente());
//		return "dipendenteForm";
//	}
//
//	@RequestMapping(value = "/dipendente/{id}", method = RequestMethod.GET)
//	public String getDipendente(@PathVariable("id") Long id, Model model) {
//		model.addAttribute("dipendente", this.dipendenteService.findById(id));
//		return "showDipendente";
//	}
//
//	@RequestMapping(value = "/dipendente", method = RequestMethod.POST)
//	public String newDipendente(@Valid @ModelAttribute("dipendente") Dipendente dipendente, 
//			Model model, BindingResult bindingResult) {
//		this.validator.validate(dipendente, bindingResult);
//
//		if (this.dipendenteService.alreadyExists(dipendente)) {
//			model.addAttribute("exists", "Dipendente already exists");
//			return "dipendenteForm";
//		}
//		else {
//			if (!bindingResult.hasErrors()) {
//				this.dipendenteService.save(dipendente);
//				model.addAttribute("dipendenti", this.dipendenteService.findAll());
//				return "dipendentiList";
//			}
//		}
//		return "dipendenteForm";
//	}

}